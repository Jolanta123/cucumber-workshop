package nl.codecentric.workshop.cucumber.definition.rest;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import nl.codecentric.workshop.cucumber.TriangleCalculationApplication;
import org.json.JSONObject;

public class RestTriangleCalculationApplication implements TriangleCalculationApplication {

    private final String urlUnderTest;
    private HttpRequestWithBody httpRequestWithBody;
    private HttpResponse<JsonNode> jsonNodeHttpResponse;

    public RestTriangleCalculationApplication(final String urlUnderTest) {
        this.urlUnderTest = urlUnderTest;
    }

    public void prepareForCalculation() {


        httpRequestWithBody = Unirest.post(this.urlUnderTest)
                .header("Content-Type", "application/json");

    }

    public void doTriangleCalculation(int side1, int side2, int side3) {

        // Build up JSON request
        final JSONObject jsonObject = new JSONObject();
        jsonObject.put("side1", side1);
        jsonObject.put("side2", side2);
        jsonObject.put("side3", side3);

        // Do actual request
        try {
            jsonNodeHttpResponse = httpRequestWithBody.body(jsonObject).asJson();
        }catch (UnirestException e){
            throw new RuntimeException(e);
        }
    }

    public String getCalculatedTriangleType() {
        final JsonNode body = jsonNodeHttpResponse.getBody();
        return body.getObject().get("triangleType").toString();
    }
}
